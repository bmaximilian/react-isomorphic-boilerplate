<h1>React Isomorphic Boilerplate</h1>
<p>
    A Boilerplate for React Redux with Server-Side Rendering
</p>
<p><span>&copy; </span>Maximilian Beck</p>
<hr/>
<br/>

<h2>Modules</h2>
<ul>
    <li>React</li>
    <li>Redux</li>
    <li>Express</li>
    <li>RxJS (actions and netflix best practices)</li>
    <li>Debug Logger with different colors and namespaces</li>
    <li>React Store wrapper with every, await, addReducer functions</li>
    <li>Services as Singleton</li>
    <li>HTTP Service with fetch<b> (missing)</b></li>
    <li>Dynamic routing (Add and remove routes after app is loaded)<b> (missing)</b></li>
    <li>Traits as Decorators<b> (missing)</b></li>
    <li>Express session handling<b> (missing)</b></li>
    <li>SCSS Styles and Gulp for asset building<b> (missing)</b></li>
</ul>
/**
 * Created on 24.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const path = require('path');

module.exports = function (dir) {
  return path.join(__dirname, '..', dir)
};

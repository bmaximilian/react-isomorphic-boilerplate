/**
 * Created on 24.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const resolve = require('./resolve');

module.exports = {
  name: 'client',
  entry: {
    app: resolve('src') + '/client/main.js'
  },
  output: {
    filename: 'main.js',
    path: resolve('dist/public/js'),
  },
  resolve: {
    extensions: ['.js', '.json'],
    alias: {
      '@': resolve('src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [resolve('src'), resolve('test')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')],
        query: {
          presets: [
            require.resolve('babel-preset-env'),
            require.resolve('babel-preset-react'),
            require.resolve('babel-preset-stage-2'),
          ],
        }
      }
    ]
  },
  plugins: [
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new FriendlyErrorsPlugin()
  ]
};

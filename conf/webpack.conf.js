/**
 * Created on 23.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const webpackServer = require('./webpack.server.conf');
const webpackClient = require('./webpack.client.conf');

module.exports = [
  webpackServer,
  webpackClient,
];

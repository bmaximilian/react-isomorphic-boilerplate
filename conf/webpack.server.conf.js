/**
 * Created on 24.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const webpack = require('webpack');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const resolve = require('./resolve');

module.exports = {
  name: 'server',
  entry: {
    app: resolve('src') + '/server/app.js'
  },
  target: "node",
  externals: [nodeExternals()],
  output: {
    filename: 'app.js',
    path: resolve('dist/server'),
  },
  resolve: {
    extensions: ['.js', '.json'],
    alias: {
      '@': resolve('src')
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [resolve('src'), resolve('test')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        include: [resolve('src'), resolve('test')],
        query: {
          presets: [
            require.resolve('babel-preset-env'),
            require.resolve('babel-preset-react'),
            require.resolve('babel-preset-stage-2'),
          ],
        }
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': process.env
    }),
    // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new FriendlyErrorsPlugin()
  ]
};

/**
 * Created on 26.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { Component } from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

/**
 * @class App
 */
class App extends Component {
  static propTypes = {
    route: PropTypes.any,
  };

  applyBasePathToRoutes(routes = this.props.route.routes, basePath = this.props.route.path) {
    return routes.map((route) => {
      const newRoute = route;
      newRoute.path = (basePath + route.path).replace(/(\/)+/g, '/');
      return newRoute;
    });
  }

  render() {
    return renderRoutes(this.applyBasePathToRoutes());
  }
}

export default App;

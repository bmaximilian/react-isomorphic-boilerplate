/**
 * Created on 19.11.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import bootHome from './modules/home/boot';

export default () => {
  bootHome();
};

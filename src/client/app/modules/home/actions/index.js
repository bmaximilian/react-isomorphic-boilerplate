/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { actions } from '../constants';

export function increment(number = 1) {
  return {
    type: actions.INCREMENT,
    payload: number,
  };
}

export function decrement(number = 1) {
  return {
    type: actions.DECREMENT,
    payload: number,
  };
}

/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Store from '../../../../lib/services/Store';
import counter from './epics/counter';

export default () => {
  if (!Store.hasEpicFor('app/home/counter')) {
    Store.addEpic(counter, 'app/home/counter');
  }
};

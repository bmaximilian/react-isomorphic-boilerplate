/**
 * Created on 25.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { get } from 'lodash';
import Store from '../../../../../lib/services/Store';
import initialState from '../state/index';
import reducer from '../reducer/index';
import { increment, decrement } from '../actions/index';

/**
 * @class Home
 */
class Home extends Component {
  /**
   * Boots the component
   */
  componentDidMount() {
    this.componentName = 'Home';

    if (!Store.hasReducerFor('home')) {
      Store.addReducer(reducer, 'home');
    }
  }

  /**
   * PropTypes of Home
   *
   * @type {{count: *, dispatch: *}}
   */
  static propTypes = {
    count: PropTypes.number,
    dispatch: PropTypes.func,
    state: PropTypes.any,
  };

  /**
   * Dispatch trigger
   *
   * @param trigger
   * @returns {string}
   */
  static trigger(trigger) {
    return `client/app/modules/home/components/Home.${trigger}`;
  }

  /**
   * Increases the counter
   */
  handleIncrement = () => {
    Store.dispatch(increment(), Home.trigger('handleIncrement'));
  };

  /**
   * Decreases the counter
   */
  handleDecrement = () => {
    Store.dispatch(decrement(), Home.trigger('handleDecrement'));
  };

  /**
   * @return {XML}
   */
  render() {
    return (
      <div>
        <h1>Hello {this.componentName}!</h1>
        <p>Count: {this.props.count}</p>
        <button onClick={this.handleIncrement}>Increment</button>
        <button onClick={this.handleDecrement}>Decrement</button>
      </div>
    );
  }
}

export default connect(state => ({
  count: get(state, 'home.counter.count', initialState.counter.count),
}))(Home);

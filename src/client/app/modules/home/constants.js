/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

export const actions = {
  INCREMENT: 'counter.increment',
  DECREMENT: 'counter.decrement',
};

export const api = {};

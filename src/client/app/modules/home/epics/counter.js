/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { debug, env } from '../../../../../lib/util/index';
import { decrement } from '../actions';
import { actions } from '../constants';

export default action$ =>
  action$
    .ofType(actions.INCREMENT)
    .delay(2000)
    .do(action => debug(`Emit counter for action of type '${action.type}'`, env.EPIC_DEBUG_NAMESPACE))
    .mapTo(decrement());

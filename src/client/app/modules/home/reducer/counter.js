/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { assign, isNumber } from 'lodash';
import { actions } from '../constants';
import initialState from '../state/counter';

/**
 * Reducer for counter
 *
 * @param state
 * @param action
 * @returns {{counter}}
 */
export default (state = initialState, action) => {
  switch (action.type) {
    case actions.INCREMENT:
      return assign({}, state, {
        count: isNumber(action.payload) ? state.count + action.payload : state.count + 1,
      });
    case actions.DECREMENT:
      return assign({}, state, {
        count: isNumber(action.payload) ? state.count - action.payload : state.count - 1,
      });
    default:
      return state;
  }
};

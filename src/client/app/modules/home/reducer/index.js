/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { combineReducers } from 'redux';
import counter from './counter';

export default combineReducers({
  counter,
});

/**
 * Created on 27.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Home from './components/Home';

export default [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/home',
    component: Home,
  },
];

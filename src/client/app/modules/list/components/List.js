/**
 * Created on 25.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React, { Component } from 'react';
import { Http } from '../../../../../lib/services/index';

/**
 * @class List
 */
class List extends Component {
  /**
   * Constructor of List
   */
  constructor() {
    super();
    this.state = {
      posts: [],
    };
  }

  /**
   * Did Mount
   */
  componentDidMount() {
    Http.setBaseURL('https://jsonplaceholder.typicode.com');
    Http.send('GET', '/posts')
      .then(({ body }) => {
        this.setState({
          posts: body,
        });
      });
  }

  /**
   * @return {XML}
   */
  render() {
    this.componentName = 'List';
    return <div>
      <h1>Hello {this.componentName}!</h1>
      <ul>
        {this.state.posts.map(post => <li key={post.id}>{post.title}</li>)}
      </ul>
    </div>;
  }
}

export default List;

/**
 * Created on 27.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import List from './components/List';

export default {
  path: '/list',
  component: List,
};

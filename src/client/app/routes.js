/**
 * Created on 27.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { flatten } from 'lodash';
import App from './App';
import Home from './modules/home/routes';
import List from './modules/list/routes';

export default {
  component: App,
  path: '/',
  routes: flatten([
    Home,
    List,
  ]),
};

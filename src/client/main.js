/**
 * Created on 23.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';
import { isServer } from '../lib/util/index';

import Store from '../lib/services/Store';
import routes from '../shared/routes';
import boot from '../shared/boot';

/* eslint-disable */
if (!isServer()) {
  boot();
  Store.createStore(window.__INITIAL_STATE__);
}

// garbage collect
delete window.__INITIAL_STATE__;
delete window.__ENV__;
/* eslint-enable */

const AppRouter = () => (
  <Provider store={Store}>
    <BrowserRouter>
      {renderRoutes(routes)}
    </BrowserRouter>
  </Provider>
);

hydrate(<AppRouter />, document.querySelector('#app'));

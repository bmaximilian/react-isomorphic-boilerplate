/**
 * Created on 12.11.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { get, has, isFunction, isString, isObject, keys, includes, set, upperCase } from 'lodash';
import { env, debug, deepDelete } from '../../lib/util/index';

const singleton = Symbol('Http-singleton-symbol');

/**
 * @class Http
 */
class Http {
  /**
   * Stores all HTTP requests
   *
   * @type {{}}
   */
  requests = {};

  /**
   * Contains all middlewares
   *
   * @type {{}}
   */
  middlewares = {};

  /**
   * Contains the request headers
   *
   * @type {{}}
   */
  headers = {};

  /**
   * Contains the base url
   *
   * @type {string}
   */
  baseUrl = '//localhost';

  /**
   * Return singleton instance
   *
   * @returns {*}
   */
  static get instance() {
    if (!this[singleton]) {
      this[singleton] = new Http();
    }

    return this[singleton];
  }

  /**
   * Send request
   *
   * @param method
   * @param url
   * @param body
   * @param headers
   * @returns {Promise}
   */
  send(method = 'GET', url = '/', body = {}, headers = {}) {
    const settedMethod = upperCase(method);
    const xhr = new XMLHttpRequest();

    // check method
    if (!isString(settedMethod) || !includes(['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'PATCH'], settedMethod)) {
      throw new Error(`Method ${isString(method) ? `${method} ` : ''}does not match GET, POST, PUT, DELETE, PATCH or OPTIONS`);
    }

    // check url
    if (!isString(url)) {
      throw new Error('Url must be a string');
    }

    const urlPath = url.slice(1).replace('/', '.');
    xhr.open(settedMethod, `${this.baseUrl}${url}`);

    // Set general headers
    keys(this.headers).forEach((key) => {
      xhr.setRequestHeader(key, this.headers[key]);
    });

    // Set custom headers
    keys(headers).forEach((key) => {
      xhr.setRequestHeader(key, headers[key]);
    });

    return new Promise((resolve, reject) => {
      xhr.onreadystatechange = () => {
        if (xhr.cancelled) {
          debug(`${settedMethod}: ${url}  -> cancelled`, env.HTTP_SEND_DEBUG_NAMESPACE);
          this.requests = deepDelete(this.requests, urlPath);
          resolve(100, null, xhr.getAllResponseHeaders());
        }

        if (xhr.readyState === 4) {
          let responseBody = xhr.responseText;
          try {
            responseBody = JSON.parse(responseBody);
          } catch (e) { /* nothing to do */ }

          if (xhr.status < 300) {
            debug(`${settedMethod}: ${url} -> resolved (${xhr.status})`, env.HTTP_SEND_DEBUG_NAMESPACE);
            this.requests = deepDelete(this.requests, urlPath);
            keys(this.middlewares).forEach((key) => {
              responseBody = this.middlewares[key](
                xhr.status,
                responseBody,
                xhr.getAllResponseHeaders(),
              );
            });
            resolve({
              status: xhr.status,
              body: responseBody,
              headers: xhr.getAllResponseHeaders(),
            });
          } else {
            debug(`${settedMethod}: ${url} -> rejected (${xhr.status})`, env.HTTP_SEND_DEBUG_NAMESPACE);
            this.requests = deepDelete(this.requests, urlPath);
            keys(this.middlewares).forEach((key) => {
              responseBody = this.middlewares[key](
                xhr.status,
                responseBody,
                xhr.getAllResponseHeaders(),
              );
            });
            reject(xhr.status, responseBody, xhr.getAllResponseHeaders());
          }
        }
      };

      xhr.cancelled = false;

      xhr.send(JSON.stringify(body));
      set(this.requests, urlPath, xhr);
      debug(`${settedMethod}: ${url} -> sent`, env.HTTP_SEND_DEBUG_NAMESPACE);
    });
  }

  /**
   * Checks if a request exists
   *
   * @param key
   * @returns {boolean}
   */
  hasRequestFor(key) {
    if (!isString(key)) {
      throw new Error('Key must be a string.');
    }

    return has(this.requests, key);
  }

  /**
   * Cancel request
   *
   * @param key
   */
  cancelRequest(key) {
    if (!this.hasRequestFor(key)) {
      throw new Error(`No request for key ${key} found.`);
    }

    const request = get(this.requests, key);

    if (request instanceof XMLHttpRequest) {
      request.cancel();
      request.cancelled = true;
      request.onreadystatechange();
      this.requests = deepDelete(this.requests, key);
    } else if (isObject(request)) {
      keys(request).forEach((k) => {
        this.cancelRequest(`${key}.${k}`);
      });
    }
  }

  /**
   * Adds a http middleware
   *
   * @param key
   * @param middleware
   */
  addMiddleware(key, middleware) {
    if (this.hasMiddlewareFor(key)) {
      throw new Error(`Middleware with key ${key} already exists`);
    }

    if (!isFunction(middleware)) {
      throw new Error(`The middleware with key ${key} is not a function`);
    }

    this.middlewares[key] = middleware;
    debug(`Added middleware with key ${key}`, env.HTTP_DEBUG_NAMESPACE);
  }

  /**
   * Checks if a middleware for the passed key is available
   *
   * @param key
   * @returns {boolean}
   */
  hasMiddlewareFor(key) {
    if (!isString(key)) {
      throw new Error('Key must be a string.');
    }

    return typeof this.middlewares[key] !== 'undefined';
  }

  /**
   * Removes the passed middleware
   *
   * @param key
   */
  removeMiddleware(key) {
    if (this.hasMiddlewareFor(key)) {
      delete this.middlewares[key];
      debug(`Removed middleware with key ${key}`, env.HTTP_DEBUG_NAMESPACE);
    }
  }

  /**
   * Adds a http header
   *
   * @param key
   * @param header
   * @param updateIfExists
   */
  addHeader(key, header, updateIfExists = true) {
    let added = true;

    if (this.hasHeaderFor(key)) {
      added = false;

      if (!updateIfExists) {
        throw new Error(`Header with key ${key} already exists`);
      }
    }

    this.headers[key] = header;
    debug(`${added ? 'Added' : 'Updated'} header with key ${key}`, env.HTTP_DEBUG_NAMESPACE);
  }

  /**
   * Checks if a header for the passed key is available
   *
   * @param key
   * @returns {boolean}
   */
  hasHeaderFor(key) {
    if (!isString(key)) {
      throw new Error('Key must be a string.');
    }

    return typeof this.headers[key] !== 'undefined';
  }

  /**
   * Removes the passed header
   *
   * @param key
   */
  removeHeader(key) {
    if (this.hasHeaderFor(key)) {
      delete this.headers[key];
      debug(`Removed header with key ${key}`, env.HTTP_DEBUG_NAMESPACE);
    }
  }

  /**
   * Sets a base (server) url
   *
   * @param base
   */
  setBaseURL(base) {
    if (!isString(base)) {
      throw new Error('Base URL must be a string.');
    }

    this.baseUrl = base;
    debug(`New base URL: ${base}`, env.HTTP_DEBUG_NAMESPACE);
  }
}

export default Http.instance;

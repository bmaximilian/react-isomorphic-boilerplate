/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import 'rxjs';
import { assign, concat, forOwn, includes, isArray } from 'lodash';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createStore as createReduxStore, applyMiddleware, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { env, debug } from '../../lib/util/index';

const singleton = Symbol('Store-singleton-symbol');
const ADD_REDUCER = 'global.storeService.addReducer';

/**
 * @class Store
 */
class Store {
  /**
   * Constructor of Store
   */
  constructor() {
    this.reduxStore = null;
    this.reducerRegistry = {};
    this.middlewareRegistry = [];
    this.enhancerRegistry = [];
    this.epicRegistry = [];
    this.epicKeys = [];
    this.actionListeners = [];

    // we add the thunk middleware for convenience
    this.addMiddleware(thunk);
  }

  static trigger(trigger) {
    return `lib/services/Store.${trigger}`;
  }

  /**
   * Returns the instance of the store
   *
   * @returns {*}
   */
  static get instance() {
    if (!this[singleton]) {
      this[singleton] = new Store();
    }

    return this[singleton];
  }

  /**
   * Creates the store
   *
   * @param initialState
   * @returns {null}
   */
  createStore = (initialState = {}) => {
    const composeEnhancers = env.NODE_ENV === 'development' ?
      composeWithDevTools
      : compose;

    // add own middleware
    this.addMiddleware(this.middleware.bind(this));

    this.reduxStore = createReduxStore(
      this.reduce.bind(this),
      initialState,
      composeEnhancers(applyMiddleware(...concat(
        this.middlewareRegistry,
        this.enhancerRegistry,
        this.epicRegistry,
      ))),
    );

    // add action listener
    this.reduxStore.subscribe(this.subscriber.bind(this));

    return this.reduxStore;
  };

  /**
   * Dispatches an action to the store
   *
   * @param action
   * @param trigger
   */
  dispatch(action, trigger = null) {
    if (!this.reduxStore) {
      throw new Error('Cannot dispatch without a redux store. You must call createStore() first.');
    }

    debug(`dispatch(${action.type})`, env.STORE_DEBUG_NAMESPACE);

    return this.reduxStore.dispatch(trigger ? assign(action, { trigger }) : action);
  }

  /**
   * Returns state
   *
   * @returns {Object}
   */
  getState() {
    if (!this.reduxStore) {
      throw new Error('Cannot get state without a redux store. You must call createStore() first.');
    }

    return this.reduxStore.getState();
  }

  /**
   * Calls the reducer in reducer registry
   *
   * @param state
   * @param action
   * @returns {*}
   */
  reduce(state, action) {
    let newState = state;

    forOwn(this.reducerRegistry, (reducer, key) => {
      if (key === '/') {
        newState = reducer(state, action);
      }

      newState = assign({}, state, {
        [key]: reducer(state[key], action),
      });
    });

    return assign({}, state, newState);
  }

  /**
   * Calls the next middleware
   *
   * @returns {function(*): function(*=)}
   */
  middleware() {
    return next => (action) => {
      this.lastAction = action;
      return next(action);
    };
  }

  /**
   * Calls the action listeners
   */
  subscriber() {
    const dispatch = this.dispatch.bind(this);
    const getState = this.getState.bind(this);
    const action = this.lastAction;

    setImmediate(() => {
      this.actionListeners.forEach((listener) => {
        listener(action, dispatch, getState);
      });
    });
  }

  /**
   * Adds an action listener
   *
   * @param listener
   * @returns {function(this:Store)}
   */
  addActionListener(listener) {
    this.actionListeners.push(listener);
    return this.removeActionListener.bind(this, listener);
  }

  /**
   * Removes an action listener
   *
   * @param listener
   */
  removeActionListener(listener) {
    const index = this.actionListeners.indexOf(listener);
    if (index !== -1) {
      this.actionListeners.splice(index, 1);
    }
  }

  /**
   * Adds a store enhancer
   * Must be called before createStore()
   *
   * @param enhancer
   * @returns {*}
   */
  addEnhancer(enhancer) {
    if (typeof enhancer !== 'function') {
      throw new Error('Enhancer must be function.');
    }

    this.enhancerRegistry.push(enhancer);

    return this;
  }

  /**
   * Adds a store middleware
   * Must be called before createStore()
   *
   * @param middleware
   * @returns {*}
   */
  addMiddleware(middleware) {
    if (typeof middleware !== 'function') {
      throw new Error('Middleware must be a function.');
    }

    this.middlewareRegistry.push(middleware);

    return this;
  }

  /**
   * Adds a store epic
   * Must be called before createStore()
   *
   * @param epic
   * @param key
   * @returns {*}
   */
  addEpic(epic, key = null) {
    if (typeof epic !== 'function') {
      throw new Error('Epic must be a function.');
    }

    if (key !== null) {
      if (this.hasEpicFor(key)) {
        throw new Error(`Epic with key "${key}" already exists.`);
      }

      this.epicKeys.push(key);
    }

    this.epicRegistry.push(createEpicMiddleware(epic));
    debug(key ? `Add epic for ${key}` : 'Add epic', env.STORE_DEBUG_NAMESPACE);

    return this;
  }

  /**
   * Checks if the epic key registry has the passed key
   *
   * @param key
   * @returns {boolean}
   */
  hasEpicFor(key) {
    return includes(this.epicKeys, key);
  }

  /**
   * Adds a store reducer
   * Can be called after createStore()
   *
   * @param reducer
   * @param key {String} - (optional) if not set, a root reducer will be added
   * @returns {*}
   */
  addReducer(reducer, key = '/') {
    if (typeof key !== 'string') {
      throw new Error('Key must be a string.');
    }

    if (this.reducerRegistry[key]) {
      throw new Error(`Reducer with key "${key}" already exists.`);
    }

    this.reducerRegistry[key] = reducer;

    // we must dispatch an action after adding a reducer to an existing store
    // only this way the reducer will attach it's intial state
    if (this.reduxStore) {
      this.dispatch({
        type: ADD_REDUCER,
        key,
      }, Store.trigger('addReducer'));
    }
    debug(key ? `Add reducer for ${key}` : 'Add reducer', env.STORE_DEBUG_NAMESPACE);

    return this;
  }

  hasReducerFor(key) {
    if (typeof key !== 'string') {
      throw new Error('Key must be a string.');
    }

    return typeof this.reducerRegistry[key] !== 'undefined';
  }

  /**
   * Removes an existing reducer
   *
   * @param key
   * @returns {Object}
   */
  removeReducer(key) {
    delete this.reducerRegistry[key];

    return this;
  }

  /**
   * Listens once for an action
   *
   * @param type {String|Array}         type(s) of successfull action
   * @param errorType {String|Array}    (optional) type(s) of unsuccessfull action
   * @returns {Promise}
   */
  await(type, errorType) {
    const types = isArray(type) ? type : [type];
    const errorTypes = (!errorType || isArray(errorType)) ? errorType : [errorType];

    debug(`await(${(types || []).join(', ')},${(errorTypes || []).join(', ')})`, env.STORE_DEBUG_NAMESPACE);

    return new Promise((resolve, reject) => {
      const removeListener = this.addActionListener((action, dispatch, getState) => {
        if (includes(types, action.type)) {
          removeListener();
          debug(`await(${(types || []).join(', ')},${(errorTypes || []).join(', ')}) -> resolved`, env.STORE_DEBUG_NAMESPACE);

          return resolve(action, dispatch, getState);
        } else if (errorTypes && includes(errorTypes, action.type)) {
          removeListener();
          debug(`await(${(types || []).join(', ')},${(errorTypes || []).join(', ')}) -> rejected`, env.STORE_DEBUG_NAMESPACE);

          return reject(action.error || action, dispatch, getState);
        }

        return undefined;
      });
    });
  }

  /**
   * Calls a function every time an action got dispatched
   *
   * @param type {String|Array}   type(s) of action
   * @param cb {Function}         Callback with the following arguments: action, dispatch , getState
   * @returns {Function}          Removes the every listener when called
   */
  every(type, cb) {
    const types = isArray(type) ? type : [type];

    debug(`every(${types.join(', ')})`, env.STORE_DEBUG_NAMESPACE);

    return this.addActionListener((action, dispatch, getState) => {
      if (includes(types, action.type)) {
        return cb(action, dispatch, getState);
      }

      return undefined;
    });
  }

  /**
   * Subscribes a callback to the store
   *
   * @param {Function} callback
   * @returns {Function}          function to unsubscribe
   */
  subscribe(callback) {
    return this.reduxStore.subscribe(callback);
  }
}

export default Store.instance;

/**
 * Created on 03.12.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import React from 'react';
import { get, includes, isObject, isString, keys } from 'lodash';
import countryCodes from './countryCodes';

const singleton = Symbol('Translate-singleton-symbol');

/**
 * @class Translate
 */
class Translate {
  /**
   * Contains all available translations
   *
   * @type {{}}
   */
  translations = {};

  /**
   * Language for fallback if current language is not found
   *
   * @type {string}
   */
  fallbackLanguage = 'en-GB';

  /**
   * Current translation language
   *
   * @type {string}
   */
  currentLanguage = 'en-GB';

  /**
   * Contains all wrapped components
   *
   * @type {Array}
   */
  wrappedComponents = [];

  /**
   * Ensures that the class is exported as singleton instance
   *
   * @returns {*}
   */
  static get instance() {
    if (!this[singleton]) {
      this[singleton] = new Translate();
    }

    return this[singleton];
  }

  /**
   * Check if the language is a valid country-code
   *
   * @param language
   * @param mustBeIncludedByTranslations
   */
  checkLanguage(language, mustBeIncludedByTranslations = true) {
    if (!isString(language)) {
      throw new Error('Language must be a string');
    }

    if (!includes(keys(countryCodes), language)) {
      throw new Error(`Language ${language} must be a valid country code`);
    }

    if (!includes(keys(this.translations), language) && mustBeIncludedByTranslations) {
      throw new Error(`No translation for language ${language} available`);
    }
  }

  /**
   * Changes the current language
   *
   * @param language
   */
  setLanguage(language) {
    this.checkLanguage(language);

    this.currentLanguage = language;

    this.wrappedComponents.forEach(c => c.forceUpdate());
  }

  /**
   * Changes the fallback language
   *
   * @param language
   */
  setFallbackLanguage(language) {
    this.checkLanguage(language);

    this.fallbackLanguage = language;
  }

  /**
   * Adds a new translation
   *
   * @param language
   * @param translations
   */
  addTranslation(language, translations) {
    this.checkLanguage(language, false);

    if (!isObject(translations)) {
      throw new Error('Translations must be an object');
    }

    this.translations[language] = translations;
  }

  /**
   * Replaces the translations array
   *
   * @param translations
   */
  setTranslations(translations) {
    if (!isObject(translations)) {
      throw new Error('Translations must be an object');
    }

    keys(translations).forEach((language) => {
      this.checkLanguage(language, false);
    });

    this.translations = translations;
  }

  /**
   * Replaces placeholder with options
   *
   * @param string
   * @param options
   * @returns {*}
   */
  static replaceOptions(string, options) {
    let returnValue = string;

    keys(options).forEach((key) => {
      returnValue = returnValue.replace(`{${key}}`, options[key]);
    });

    return returnValue;
  }

  /**
   * Translates a key
   *
   * @param key
   * @param options
   * @returns {*}
   */
  translate(key, options) {
    return Translate.replaceOptions(
      get(
        this.translations[this.currentLanguage],
        key,
        get(
          this.translations[this.fallbackLanguage],
          key,
          key,
        ),
      ),
      options,
    );
  }

  /**
   * Maps translations to properties
   *
   * @param WrappedComponent
   * @return {function}
   */
  withTranslations(WrappedComponent) {
    const WithTranslations = props => <WrappedComponent
      {...props}
      t={this.translate.bind(this)}
      ref={(c) => { this.wrappedComponents.push(c); }}
    />;

    return WithTranslations;
  }
}

export default Translate.instance;

/**
 * Created on 30.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Debug from 'debug';
import { env } from './index';

/**
 * Writes a debug log to the console
 *
 * @param message
 * @param namespace
 */
export default (message, namespace = env.DEBUG_NAMESPACE) => {
  const debug = Debug(namespace);
  debug.enabled = env.DEBUG_ENABLED;
  debug(message);
};

export const consoleLog = (...args) => {
  /* eslint-disable */
  args.forEach(arg => console.log(`${env.DEBUG_NAMESPACE}: `, arg));
  /* eslint-enable */
};

export const consoleError = (...args) => {
  /* eslint-disable */
  args.forEach(arg => console.error(`${env.DEBUG_NAMESPACE}: `, arg));
  /* eslint-enable */
};

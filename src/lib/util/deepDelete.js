/**
 * Created on 12.11.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

const deepDelete = (object, key) => {
  const keys = key.split('.');
  let newObj = object;

  if (keys.length > 1) {
    newObj = deepDelete(keys.slice(1).join('.'), newObj[keys[0]]);
  } else {
    delete newObj[key];
  }

  return newObj;
};

export default deepDelete;

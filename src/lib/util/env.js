/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import isServer from './isServer';

/* eslint-disable */
export default isServer() ? process.env : window.__ENV__;
/* eslint-enable */

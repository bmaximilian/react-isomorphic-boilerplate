/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import isServer from './isServer';
import env from './env';
import deepDelete from './deepDelete';
import debug, { consoleError, consoleLog } from './debug';

export {
  isServer,
  env,
  debug,
  deepDelete,
  consoleError,
  consoleLog,
};

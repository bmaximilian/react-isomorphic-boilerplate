/**
 * Created on 23.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Express from 'express';
import path from 'path';
import fs from 'fs';
import BodyParser from 'body-parser';
import CookieParser from 'cookie-parser';
import { consoleError, debug } from '../lib/util/index';
import './env';
import Router from './router';
import Logger from './logger';

const app = Express();
const babelCacheFile = path.join(process.env.HOME, '.babel.json');
const publicDir = path.join(__dirname, '..', 'public');

// Clear babel cache in production to prevent memory leaks
if (process.env.NODE_ENV === 'production' && fs.existsSync(babelCacheFile)) {
  try {
    fs.unlinkSync(babelCacheFile);
  } catch (e) {
    consoleError(e);
  }
}

// Setup view engine (pug)
app.set('views', path.join(publicDir, 'views'));
app.set('view engine', 'pug');

// Setup common expressJs modules
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: false }));
app.use(CookieParser());
app.use(Express.static(publicDir));

// Setup own modules
app.use(Logger);
app.use('/', Router);
// Setup project specific modules

// Start the server
app.listen(process.env.PORT);
debug(`Node Server listening on Port ${process.env.PORT}`);

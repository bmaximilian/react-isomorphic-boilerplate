/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

process.env.PORT = process.env.PORT || 8080;
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.DEBUG_NAMESPACE = 'frontend:server';
process.env.STORE_DEBUG_NAMESPACE = 'frontend:store';
process.env.EPIC_DEBUG_NAMESPACE = 'frontend:epic';
process.env.HTTP_DEBUG_NAMESPACE = 'frontend:http';
process.env.HTTP_SEND_DEBUG_NAMESPACE = 'frontend:http:send';
process.env.DEBUG_ENABLED = true;

/**
 * Created on 26.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import debug from '../lib/util/debug';

export default (req, res, next) => {
  debug(`${req.method}: ${req.originalUrl}`);
  next();
};

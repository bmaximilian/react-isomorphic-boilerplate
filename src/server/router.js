/**
 * Created on 24.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import Express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';
import { Provider } from 'react-redux';

import Store from '../lib/services/Store';
import routes from '../shared/routes';
import boot from '../shared/boot';

const router = Express.Router();
// add middleware and devtools
boot();
Store.createStore();

router.get('*', (req, res) => {
  const context = {};
  const content =
    renderToString(<Provider store={Store}>
      <StaticRouter location={req.url} context={context}>
        {renderRoutes(routes)}
      </StaticRouter>
    </Provider>);

  if (context.status === 404) {
    res.status(404);
  }

  if (context.status === 302) {
    return res.redirect(302, context.url);
  }

  return res.render(
    'index',
    {
      title: 'Express',
      data: Store.getState(),
      content,
      env: {
        NODE_ENV: process.env.NODE_ENV,
        DEBUG_ENABLED: process.env.DEBUG_ENABLED,
        DEBUG_NAMESPACE: process.env.DEBUG_NAMESPACE,
        STORE_DEBUG_NAMESPACE: process.env.STORE_DEBUG_NAMESPACE,
        EPIC_DEBUG_NAMESPACE: process.env.EPIC_DEBUG_NAMESPACE,
        HTTP_SEND_DEBUG_NAMESPACE: process.env.HTTP_SEND_DEBUG_NAMESPACE,
        HTTP_DEBUG_NAMESPACE: process.env.HTTP_DEBUG_NAMESPACE,
      },
    },
  );
});

export default router;

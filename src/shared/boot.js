/**
 * Created on 19.11.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import bootApp from '../client/app/boot';

export default () => {
  bootApp();
};

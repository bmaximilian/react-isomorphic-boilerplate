/**
 * Created on 31.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import boot from './boot';
import routes from './routes';

export {
  boot,
  routes,
};

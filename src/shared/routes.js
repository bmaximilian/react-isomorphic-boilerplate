/**
 * Created on 23.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

import { flatten } from 'lodash';
import App from '../client/app/routes';

const routes = [
  App,
];

export default flatten(routes);

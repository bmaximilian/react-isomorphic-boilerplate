/**
 * Created on 23.10.17.
 *
 * @author Maximilian Beck <maximilian.beck@wtl.de>
 */

describe('Test configuration', () => {
  it('Should run tests', () => {
    expect('Hello World').to.equal('Hello World');
  });
});
